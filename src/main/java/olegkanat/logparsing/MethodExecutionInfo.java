package olegkanat.logparsing;

import java.util.Date;

/**
 * Created by Олег on 25.06.2017.
 * @author Kanatnikov Oleg
 *
 * This class represent one method execution
 */
public class MethodExecutionInfo {
    private Date entryTime;
    private Date exitTime;
    private String methodName;
    private int operationId;
    private long executingTime;

    /**
     * Factory method to create method info using logs objects.
     * It is not required to use factory here but it remains to
     * maintain the same style of code.
     */
    public static MethodExecutionInfo createInfoFromEntryAndExitLogs(TestLog entryLog, TestLog exitLog){
        return new MethodExecutionInfo(entryLog, exitLog);
    }

    public String getMethodName() {
        return methodName;
    }

    public int getOperationId() {
        return operationId;
    }

    public long getExecutingTime() {
        return executingTime;
    }

    public String toString(){
        return "executingTime: " + executingTime + ". methodName: " + methodName + ". operationId: " + operationId;
    }

    private MethodExecutionInfo(TestLog entryLog, TestLog exitLog){
        this.entryTime = entryLog.getTime();
        this.exitTime = exitLog.getTime();
        this.methodName = entryLog.getMethodName();
        this.operationId = entryLog.getOperationId();
        this.executingTime = exitLog.getTime().getTime() - entryLog.getTime().getTime();
    }
}
