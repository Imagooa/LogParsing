package olegkanat.logparsing;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by Олег on 25.06.2017.
 * @author Kanatnikov Oleg
 *
 * This class only represent one line from log file as Object.
 */
public class TestLog {

    private static final DateFormat TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss,SSS");
    public static final Pattern LOG_TEMPLATE =
            Pattern.compile("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2},\\d{3}\\sTRACE\\s\\[OperationsImpl\\]\\s(entry|exit)\\swith\\s\\(.+:\\d+\\)$");

    /**
     * Logs have to be sorted by id in ascending order. If ids are the same, the entry log comes before exit log
     * */
    public static final Comparator<TestLog> SORT_BY_ID = (log1, log2) -> {
        if (log1.operationId == log2.operationId)
            return (log1.operationType.equals("entry"))? -1 : 1;
        else
            return (log1.operationId < log2.operationId)? -1 : 1;
    };

    private Date time;
    private String operationType;
    private String methodName;
    private int operationId;

    /**
     * To avoid high coupling of code static factory method is used
     * */
    public static TestLog stringToTestLog(String logLine){
        String[] tmpArr = logLine.split(" ");
        TestLog log = new TestLog();
        try {
            log.time = TIME_FORMAT.parse(tmpArr[0]);
        } catch (ParseException exc){
            System.out.println("Cannot parse time in this string: " + logLine);
        }
        log.operationType = tmpArr[3];
        String methodInfo = tmpArr[5].substring(1, tmpArr[5].length()-1);
        log.methodName = methodInfo.substring(0, methodInfo.indexOf(":"));
        log.operationId = Integer.parseInt(methodInfo.substring(methodInfo.indexOf(":")+1));
        return log;
    }

    /**
     * Compares two logs if they have equal method name and id
     * */
    public boolean ofTheSameMethod(TestLog log){
        boolean theMethodIsTheSame = this.getMethodName().equals(log.getMethodName());
        boolean theEntryIsTheSame = this.getOperationId() == log.getOperationId();

        return (theMethodIsTheSame && theEntryIsTheSame);
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }

    public String toString(){
        return "Time: " + time + ". operationType: " + operationType + ". methodName: " + methodName + ". operationId: " + operationId;
    }
    /**
     * To avoid high coupling of code constructor is private
     * */
    private TestLog(){
        this.time = new Date(0);
        this.operationType = "undefined";
        this.methodName = "undefined";
        this.operationId = -1;
    }
}
