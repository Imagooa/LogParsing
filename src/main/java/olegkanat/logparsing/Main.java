package olegkanat.logparsing;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Олег on 25.06.2017.
 * @author Kanatnikov Oleg
 */
public class Main {

    public static final String DEFAULT_FILE_PATH = "testlog.log";
    public static final PrintStream OUT = System.out;

    private static BufferedReader br = null;

    public static void main(String[] args) throws FileNotFoundException{
        try{
            String logFileName = args[0];
            br = new BufferedReader(new FileReader(logFileName));
        } catch (FileNotFoundException exc){
            OUT.println("The file doesn't exist or incorrect");
            OUT.println("Trying default file...");
            br = new BufferedReader(new InputStreamReader(Main.class.getClassLoader().getResourceAsStream(DEFAULT_FILE_PATH)));
        } catch (ArrayIndexOutOfBoundsException exc){
            OUT.println("Please, provide file name.");
            OUT.println("Trying default file...");
            br = new BufferedReader(new InputStreamReader(Main.class.getClassLoader().getResourceAsStream(DEFAULT_FILE_PATH)));
        }
        /*Get List of sorted logs by id and method name*/
        List<TestLog> logList = br  .lines()
                                    .filter(line -> TestLog.LOG_TEMPLATE.matcher(line).matches())
                                    .map(line -> TestLog.stringToTestLog(line))
                                    .sorted(TestLog.SORT_BY_ID)
                                    .collect(Collectors.toList());

        /*Trying to find corresponded log lines with the same method name and id.
        * Add single method info to result processor*/
        for (int i=1; i < logList.size(); i++) {
            TestLog entryLog = logList.get(i-1);
            TestLog exitLog = logList.get(i);
            if (entryLog.ofTheSameMethod(exitLog)){
                MethodProcessingResult.addMethodExecutionInfo(MethodExecutionInfo.createInfoFromEntryAndExitLogs(entryLog, exitLog));
                i++;
            } else {
                MethodProcessingResult.addNoPairTestLog(entryLog);
            }

        }

        /*Get final results and print*/
        ArrayList<MethodProcessingResult> results = MethodProcessingResult.getResults();
        for (MethodProcessingResult result: results) System.out.println(result);
    }
}
