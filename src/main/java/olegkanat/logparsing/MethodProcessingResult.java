package olegkanat.logparsing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Олег on 25.06.2017.
 * @author Kanatnikov Oleg
 *
 * This class represents calculated statistics by different methods.
 * The class has no public constructors and creates instances itself.
 * All previously created instances stored in hasmap results with method name as a key.
 * Statistics can be obtained at any time, external classes just need to add
 * info about method execution and then get results when it is required.
 *
 * Currently the direct access to fields is not provided because the task doesn't require it.
 * In external classes objects might be cast to string and printed.
 * To provide access, getters and setters might be added.
 */
public class MethodProcessingResult {
    /*This map contains all previously created MethodProcessingResult objects*/
    private static HashMap<String, MethodProcessingResult> results = new HashMap<String, MethodProcessingResult>();

    /**
     * Additional storage for log ids with only entry or exit in method.
     * It is not required in current version
     * */
    private HashSet<Integer> noPairIds;
    private String methodName;
    private long minTime;
    private long maxTime;
    private long summTime;
    private long averageTime;
    private int numberOfInvocations;
    private int idOfTheLongestInvocation;

    /**
     * When adding new info into class the results of calculations are updated
     * */
    public static void addMethodExecutionInfo(MethodExecutionInfo methodExecutionInfo){
        MethodProcessingResult mpr = getByMethodName(methodExecutionInfo.getMethodName());
        long operationExecTime = methodExecutionInfo.getExecutingTime();
        mpr.minTime = (mpr.minTime < operationExecTime) ? mpr.minTime : operationExecTime;
        mpr.maxTime = (mpr.maxTime > operationExecTime) ? mpr.maxTime : operationExecTime;
        mpr.idOfTheLongestInvocation = (mpr.maxTime > operationExecTime) ? mpr.idOfTheLongestInvocation : methodExecutionInfo.getOperationId();
        mpr.summTime += operationExecTime;
        mpr.numberOfInvocations ++;
        mpr.averageTime = mpr.summTime / mpr.numberOfInvocations;

    }

    /**
     * External class finds log without pair and adds it into storage
     * */
    public static void addNoPairTestLog(TestLog log){
        MethodProcessingResult mpr = getByMethodName(log.getMethodName());
        mpr.noPairIds.add(log.getOperationId());
    }

    /**
     * Method for getting the newest statistics. The results updated every time when new info is added into this class
     * */
    public static ArrayList<MethodProcessingResult> getResults(){
        return new ArrayList<MethodProcessingResult>(results.values());
    }

    public String toString(){
        return  "OperationsImpl:" + this.methodName +
                " min " + this.minTime +
                ", max " + this.maxTime +
                ", avg " + this.averageTime +
                ", max id " + this.idOfTheLongestInvocation +
                ", count " + this.numberOfInvocations;
    }
    /**
     * Only the class itself is able to create new objects
     * */
    private MethodProcessingResult(String methodName){
        this.noPairIds = new HashSet<Integer>();
        this.methodName = methodName;
        this.minTime = Long.MAX_VALUE;
        this.maxTime = 0;
        this.summTime = 0;
        this.averageTime = -1;
        this.numberOfInvocations = 0;
        this.idOfTheLongestInvocation = -1;
    }
    /**
     * When object is requested the method first seeks the existing one in hashmap. If method doesn't
     * find it, it creates the new one, adds it into hashmap and then returns it.
     * */
    private static MethodProcessingResult getByMethodName(String methodName){
        if (!results.containsKey(methodName)) results.put(methodName, new MethodProcessingResult(methodName));
        return results.get(methodName);
    }
}
